Jest to mój dawny projekt, którego celem było zaimplementowanie dwóch algorytmów:  
- tworzący labirynt,  
- rozwiązujący stworzony labirynt.  

Program został zrealizowany czysto rozrywkowo, więc proszę nie spodziewać się przemyślanej architektury. :)

### Sterowanie
S - startuje algorytm rozwiązujący,  
Strzałki góra/dół - przyspieszanie/zwalnianie animacji,  
ESC - wyjście.  

![Picture](images/pic.png)