#include "Cell.h"

Cell::Cell()
{
	x = 0;
	y = 0;
	color = olc::WHITE;

	//generator
	visited = false;

	//solver
	parent = nullptr;
	mapped = false;
	localGoal = 0;
	globalGoal = 1000000;
}

Cell::Cell(int x, int y, olc::Pixel color)
{
	this->x = x;
	this->y = y;
	this->color = color;
}

Cell::~Cell()
{

}

int Cell::getX() { return x; };
int Cell::getY() { return y; };
void Cell::setX(int x) { this->x = x; }
void Cell::setY(int y) { this->y = y; }
bool Cell::isVisited() { return visited; }
void Cell::setVisited(bool v) { visited = v; }

void Cell::pushLink(Cell* l)
{
	links.push_back(l);
}

std::vector<Cell*> Cell::getLinks() {
	return links;
}

int Cell::distanceTo(int x, int y) {
	return abs(this->x - x) + abs(this->y - y);
}

const olc::Pixel Cell::getColor() {
	return color;
}

void Cell::setColor(olc::Pixel color) {
	this->color=color;
}

//SOLVER
bool Cell::isMapped() {
	return mapped;
}

void Cell::setMapped(bool mapped) {
	this->mapped = mapped;
}

int Cell::getLocalGoal() {
	return localGoal;
}

void Cell::setLocalGoal(int localGoal) {
	this->localGoal = localGoal;
}

int Cell::getGlobalGoal() {
	return globalGoal;
}

void Cell::setGlobalGoal(int globalGoal) {
	this->globalGoal = globalGoal;
}

Cell* Cell::getParent() {
	return parent;
}

void Cell::setParent(Cell* parent) {
	this->parent = parent;
}
