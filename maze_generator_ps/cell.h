#pragma once
#include "olcPixelGameEngine.h"
#include <vector>

class Cell {
private:
	int x;
	int y;
	olc::Pixel color;

	//generator
	std::vector<Cell*> links;
	bool visited;

	//solver
	Cell* parent;
	bool mapped;
	int localGoal;
	int globalGoal;

public:
	Cell();
	Cell(int x, int y, olc::Pixel color);
	~Cell();

	int distanceTo(int x, int y);
	int getX();
	int getY();
	const olc::Pixel getColor();
	void setColor(olc::Pixel color);
	bool isVisited();
	void setX(int x);
	void setY(int y);
	void setVisited(bool v);

	void pushLink(Cell *l);
	std::vector<Cell*> getLinks();

	//solver
	bool isMapped();
	void setMapped(bool mapped);
	int getLocalGoal();
	void setLocalGoal(int localGoal);
	int getGlobalGoal();
	void setGlobalGoal(int globalGoal);
	Cell* getParent();
	void setParent(Cell* parent);
};

