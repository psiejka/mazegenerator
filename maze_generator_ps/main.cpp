#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"
#include "Maze.h"

int main() {
	Maze maze;

	if (maze.Construct(960, 540, 1, 1)) {
		maze.Start();
	}

	return 0;
}