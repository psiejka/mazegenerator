#include "Maze.h"
#include <algorithm>
#include <thread>
#include <chrono>
#include <limits>
#include <future>

Maze::Maze()
{
	sAppName = "Maze";
}

Maze::~Maze()
{
	m_mazeThread.join();
	m_solveThread.join();
	delete[] m_maze;
	while (!m_stack.empty()) {
		Cell* tmp = m_stack.top();
		delete tmp;
		m_stack.pop();
	}
}

int Maze::offset(int x, int y) {
	return (y * m_nMazeWidth + x);
}

std::vector<Cell*> Maze::getNeighbours(Cell &cell) {
	std::vector<Cell*> tmp;
	auto offset = [&](int x, int y)
	{
		return ((cell.getY() + y) * m_nMazeWidth + (x + cell.getX()));
	};

	if (cell.getX() > 0)
		tmp.push_back(&m_maze[offset(-1, 0)]);
	if (cell.getY() > 0)
		tmp.push_back(&m_maze[offset(0, -1)]);
	if (cell.getX() + 1 < m_nMazeWidth)
		tmp.push_back(&m_maze[offset(+1, 0)]);
	if (cell.getY() + 1 < m_nMazeHeight)
		tmp.push_back(&m_maze[offset(0, +1)]);

	return tmp;
}

bool Maze::OnUserCreate() {
	Clear(olc::BLACK);

	srand(time(NULL));

	// Maze parameters
	int size = 20;
	m_nMazeWidth = int(floor(ScreenWidth() / size));
	m_nMazeHeight = int(floor(ScreenHeight() / size));
	m_maze = new Cell[m_nMazeWidth * m_nMazeHeight];
	memset(m_maze, 0x00, m_nMazeWidth * m_nMazeHeight * sizeof(int));
	m_nCellSize = int(floor(m_nMazeWidth / m_nMazeHeight) * size);

	for (int i = 0; i < m_nMazeWidth; i++) {
		for (int j = 0; j < m_nMazeHeight; j++) {
			m_maze[offset(i, j)].setX(i);
			m_maze[offset(i, j)].setY(j);
		}
	}

	// Choose a starting cell
	int x_start = rand() % m_nMazeWidth;
	int y_start = rand() % m_nMazeHeight;
	m_start = &m_maze[offset(x_start, y_start)];
	m_start->setColor(olc::GREEN);
	m_stack.push(&m_maze[offset(x_start, y_start)]);

	//solver
	int x_end = rand() % m_nMazeWidth;
	int y_end = rand() % m_nMazeHeight;
	m_end = &m_maze[offset(x_end, y_end)];
	m_nodes.push_back(&m_maze[offset(x_start, y_start)]);

	m_moveSpeed = 10;
	m_mazeThread = std::thread(&Maze::generateMaze, this);

	isGenDone = false;

	return true;
}

bool Maze::OnUserDestroy() {
	exit(0);
	return true;
}

bool Maze::OnUserUpdate(float fElapsedTime) {
	render();
	if (GetKey(olc::S).bReleased) {
		if (isGenDone) {
			m_solveThread = std::thread(&Maze::solveMaze, this);
			isGenDone = false;
		}
	}
	if (GetKey(olc::ESCAPE).bReleased)
	{
		exit(0);
	}
	if (GetKey(olc::DOWN).bReleased)
	{
		if (m_moveSpeed != 0) {
			m_moveSpeed -= 1;
			system("CLS");
			std::cout << "moveSpeed: " << (int)m_moveSpeed << "\n";
		}
	}
	if (GetKey(olc::UP).bReleased)
	{
		if (m_moveSpeed <= int(std::numeric_limits<byte>::max())) {
			m_moveSpeed += 1;
			system("CLS");
			std::cout << "moveSpeed: " << (int)m_moveSpeed << "\n";
		}
	}
	return true;
}

void Maze::generateMaze() {
	srand(time(NULL));

	olc::Pixel cTmp;
	Cell* randomChoice;

	while (m_stack.size() > 0) {
		Cell* top = m_stack.top();
		top->setVisited(true);
		std::vector<Cell*> neighbours = getNeighbours(*top);
		for (int i = neighbours.size() - 1; i >= 0; i--) {
			if (neighbours[i]->isVisited()) {
				neighbours.erase(neighbours.begin() + i);
			}
		}
		if (neighbours.size() > 0) {
			randomChoice = neighbours[int(floor(rand() % neighbours.size()))];
			cTmp = randomChoice->getColor();
			randomChoice->setColor(olc::DARK_MAGENTA);
			top->pushLink(randomChoice);
			m_stack.push(randomChoice);
			std::this_thread::sleep_for(std::chrono::milliseconds((int)m_moveSpeed));
			randomChoice->setColor(cTmp); // tmp
		}
		else
			m_stack.pop();
	}
	isGenDone = true;
}

void Maze::render() {
	int tmp1;
	int tmp2;
	olc::Pixel tmpP;
	for (int x = 0; x < m_nMazeWidth*m_nMazeHeight; x++) {
		for (int i = m_maze[x].getLinks().size() - 1; i >= 0; i--) {
			tmp1 = (m_maze[x].getLinks().at(i)->getX() * m_nCellSize) - (m_maze[x].getX() * m_nCellSize);
			tmp2 = (m_maze[x].getLinks().at(i)->getY() * m_nCellSize) - (m_maze[x].getY() * m_nCellSize);
			if (!m_maze[x].getLinks().at(i)->isMapped())
				tmpP = olc::WHITE;
			else
				tmpP = olc::BLUE;
			if (tmp1 != 0) {
				if (tmp1 > 0)
					FillRect(m_maze[x].getX() * m_nCellSize + m_nCellSize / 2, m_maze[x].getY() * m_nCellSize, m_nCellSize / 2, m_nCellSize / 2, tmpP);
				else if (tmp1 < 0)
					FillRect(m_maze[x].getX() * m_nCellSize - m_nCellSize / 2, m_maze[x].getY() * m_nCellSize, m_nCellSize / 2, m_nCellSize / 2, tmpP);
			}
			else if (tmp2 != 0) {
				if (tmp2 > 0)
					FillRect(m_maze[x].getX() * m_nCellSize, m_maze[x].getY() * m_nCellSize + m_nCellSize / 2, m_nCellSize / 2, m_nCellSize / 2, tmpP);
				else if (tmp2 < 0)
					FillRect(m_maze[x].getX() * m_nCellSize, m_maze[x].getY() * m_nCellSize - m_nCellSize / 2, m_nCellSize / 2, m_nCellSize / 2, tmpP);
			}
		}
		//NODES
		FillRect(m_maze[x].getX() * m_nCellSize,
			m_maze[x].getY() * m_nCellSize,
			m_nCellSize / 2,
			m_nCellSize / 2,
			m_maze[x].getColor());
	}
}

void Maze::solveMaze() {
	m_end->setColor(olc::RED);
	while (m_nodes.size() != 0 && !(m_end->isMapped())) {
		Cell* current = m_nodes[0];
		if (!(current->isMapped())) {
			for (int i = current->getLinks().size() - 1; i >= 0; i--) {
				m_nodes.push_back(current->getLinks().at(i));
				if (current->getLocalGoal() + 1 > current->getLinks().at(i)->getLocalGoal()) {
					current->getLinks().at(i)->setParent(current);
					current->getLinks().at(i)->setLocalGoal(current->getLinks().at(i)->getLocalGoal() + 1);
				}
				current->getLinks().at(i)->setGlobalGoal(current->getLinks().at(i)->getLocalGoal() + 1);
			}
			current->setMapped(true);
			if (current != m_start && current != m_end)
				current->setColor(olc::BLUE);
		}
		m_nodes.erase(m_nodes.begin());
		std::sort(m_nodes.begin(), m_nodes.end(), [](Cell* a, Cell* b)
			{
				return a->getGlobalGoal() < b->getGlobalGoal();
			});
		std::this_thread::sleep_for(std::chrono::milliseconds((int)m_moveSpeed));

	}

}