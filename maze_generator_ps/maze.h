#pragma once
#include "olcPixelGameEngine.h"
#include <stack>
#include "Cell.h"
#include <future>

class Maze : public olc::PixelGameEngine
{
private:
	int  m_nMazeWidth;
	int  m_nMazeHeight;
	Cell *m_maze;

	// Algorithm variables
	std::stack<Cell*> m_stack;
	int m_nCellSize;

	//Solver
	Cell* m_start;
	Cell* m_end;
	std::vector<Cell*> m_nodes;

	//Threads
	byte m_moveSpeed;
	std::thread m_mazeThread;
	std::thread m_solveThread;

	bool isGenDone;

public:
	Maze();
	~Maze();
	bool OnUserCreate() override;
	bool OnUserDestroy() override;
	virtual bool OnUserUpdate(float fElapsedTime) override;

	std::vector<Cell*> getNeighbours(Cell &cell);
	void generateMaze();
	void solveMaze();
	void render();
	int offset(int x, int y);
};

